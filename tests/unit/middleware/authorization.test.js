const { types } = require('joi');
const { Types } = require('mongoose');
const auth = require('../../../src/middleware/authorization');
const { User } = require('../../../src/models/users');

describe('auth middleware', () => {
    it('should populate req body with the jwt payload of a valid token ', () => {
        const id = new Types.ObjectId().toHexString();
        const token = new User({
            _id: id,
            name: 'userone',
        }).generateAuthenToken();

        req = {
            header: jest.fn().mockReturnValue(token),
        };
        res = {};
        next = jest.fn();

        auth(req, res, next);

        expect(req.user).toBeDefined();
        expect(req.user).toMatchObject({ _id: id });
    });
});
