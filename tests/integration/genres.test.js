const request = require('supertest');
const { Types } = require('mongoose');
const { Genre } = require('../../src/models/genres');
const { User } = require('../../src/models/users');

let server;

describe('/api/genres', () => {
    beforeEach(async () => {
        server = require('../../src/index');
        await Genre.deleteMany({});
    });
    afterEach(() => {
        server.close();
    });

    // afterAll(() => { processRestore = null }); /* ?? */

    describe('GET /', () => {
        it('should return all genres', async () => {
            await Genre.insertMany([{ name: 'genre1' }, { name: 'genre2' }]);

            const res = await request(server).get('/api/genres');

            expect(res.status).toBe(200);
            expect(res.body.length).toBe(2);

            // expect(res.body.some((genre) => {
            //         return genre.name === 'genre1'; })).toBeTruthy();
            expect(res.body[0]).toMatchObject({ name: 'genre1' });
            expect(res.body[1]).toMatchObject({ name: 'genre2' });
        });
    });

    describe('GET /:id', () => {
        it('should return a 400 error when the id is not a valid mongoose ObjectId', async () => {
            const res = await request(server).get('/api/genres/1');

            expect(res.status).toBe(400);
        });

        it('should return a 404 error when the genre with the provided id is not found in the DB ', async () => {
            const res = await request(server).get(
                `/api/genres/${new Types.ObjectId().toString()}`
            );

            expect(res.status).toBe(404);
        });

        it('should return a genre when a valid id is provided', async () => {
            /* prepare the test */
            const id = new Types.ObjectId().toString();
            const genre = new Genre({ _id: id, name: 'genre1' });
            await genre.save();

            const res = await request(server).get('/api/genres/' + id);

            expect(res.status).toBe(200);
            expect(res.body).toMatchObject({ _id: id, name: 'genre1' });
        });
    });

    describe('POST /', () => {
        let token;
        let name;

        beforeEach(() => {
            token = new User().generateAuthenToken();
            name = 'genre1';
        });

        const exec = async () => {
            return await request(server)
                .post('/api/genres')
                .set('x-auth-token', token)
                .send({ name });
        };

        it('should return a 401 error when a token is not provided ', async () => {
            token = '';
            const res = await exec();

            expect(res.status).toBe(401);
        });

        it('should return a 400 error when the name is less than 3 characters ', async () => {
            name = 'a';
            const res = await exec();

            expect(res.status).toBe(400);
        });

        it('should return a 400 error when the name is more than 20 characters ', async () => {
            name = new Array(30).join('a');
            const res = await exec();

            expect(res.status).toBe(400);
        });

        it('should save a genre if it is valid', async () => {
            const res = await exec();

            /* verify the genre was saved in the database */
            const genre = await Genre.findOne({ name: 'genre1' });
            expect(genre).toMatchObject({ name: 'genre1' });

            /* verify the response */
            expect(res.status).toBe(200);
            expect(res.body).toMatchObject({ name: 'genre1' });
        });
    });
});
