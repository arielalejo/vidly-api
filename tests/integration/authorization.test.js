const request = require('supertest');
const { Genre } = require('../../src/models/genres');
const { User } = require('../../src/models/users');

let server;
describe('authorization middleware through genres endpoint', () => {
    beforeEach(async () => {
        server = require('../../src/index');
        await Genre.deleteMany({});
    });
    afterEach(async () => {
        server.close();
    });

    let token;

    const exec = () => {
        return request(server)
            .post('/api/genres')
            .set('x-auth-token', token)
            .send({ name: 'genre1' });
    };

    it('should return 401 if token is not provided ', async () => {
        token = '';
        const res = await exec();

        expect(res.status).toBe(401);
    });

    it('should return 400 if token provided is an invalid token ', async () => {
        token = '12';
        const res = await exec();

        expect(res.status).toBe(400);
    });

    it('should return 200 if token is valid ', async () => {
        /* in this case we need to access the req, res http objects, 
           supertest cant access the req, res http objects 
           so we need to mock them => we need a unit test  */

        /* here we only can test the status code from the response */
        token = new User().generateAuthenToken();
        const res = await exec();
        expect(res.status).toBe(200);
    });
});
