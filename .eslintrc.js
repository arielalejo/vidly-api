module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: ["standard"],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly",
  },
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaVersion: 2018,
  },
  plugins: ["@typescript-eslint"],
  rules: {
    indent: ["error", 4],
    "linebreak-style": ["error", "unix"],
    quotes: ["error", "single"],
    semi: ["error", "always"],
    "no-cond-assign": ["error", "always"],
    "no-console": "off",
    "no-unused-vars": "off",
    "multiline-comment-style": ["error", "separate-lines"],
    "no-warning-comments": "error",
    "no-inline-comments": "error",
  },
  env: {
    jest: true,
  },
};
