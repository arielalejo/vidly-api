const express = require('express');
const winston = require('winston');
const config = require('config');
const logging = require('./startup/logging');
const appRoutes = require('./startup/routes');
const setValidation = require('./startup/validation');
const verifyConfig = require('./startup/config');
const startDB = require('./startup/db');

const app = express();

logging();
verifyConfig();
setValidation();
startDB();
appRoutes(app);

const port = config.get('port') || 3003;
const server = app.listen(port, '0.0.0.0', () =>
    winston.info(`listening on port ${port} ...`)
);

module.exports = server;
