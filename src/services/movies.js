const { Movie } = require('../models/movies');
const { Genre } = require('../models/genres');
const { validateId, validateMovie } = require('./requestValidation');

async function getMovies() {
    return await Movie.find().sort({ name: 1 });
}

async function getMovie(id) {
    const { error } = validateId(id);
    if (error) throw { status: 400, message: 'id pattern is incorrect' };

    const movie = await Movie.findById(id);
    if (!movie)
        throw { status: 404, message: 'movie with the given id not found' };
    return movie;
}

async function createMovie(body) {
    const { error } = validateMovie(body);
    if (error) throw { status: 400, message: error.details[0].message };

    const genre = await Genre.findOne({ _id: body.genreId });
    if (!genre)
        throw { status: 404, message: 'genre does not exist in catalog' };

    const movie = new Movie({
        title: body.title,
        numberInStock: body.numberInStock,
        dailyRentalRate: body.dailyRentalRate,
        genre: {
            _id: genre._id,
            name: genre.name,
        },
    });
    return await movie.save();
}
 
async function updateMovie(id, body) {
	
    const result = validateId(id);
    
    if (result.error) throw { status: 400, message: 'id pattern is incorrect' };

    const genre = await Genre.findById(body.genreId);
    if (!genre)
        throw { status: 400, message: 'genre with the given id was not found' };

    const { error } = validateMovie(body);
    if (error) throw { status: 400, message: error.details[0].message };

    const movie = await Movie.findById(id);
    if (!movie)
        throw { status: 404, message: 'movie with the given id not found' };

    movie.set({
        title: body.title,
        numberInStock: body.numberInStock
            ? body.numberInStock
            : movie.numberInStock,
        dailyRentalRate: body.dailyRentalRate
            ? body.dailyRentalRate
            : movie.dailyRentalRate,
        genre: {
            _id: genre._id,
            name: genre.name,
        },
    });

    return await movie.save();
}

async function deleteMovie(id) {
	console.log('id', id);
    const { error } = validateId(id);
    if (error) throw { status: 400, message: 'id pattern is incorrect' };

    const movie = await Movie.findByIdAndDelete(id);
    if (!movie)
        throw { status: 404, message: 'movie with the given id not found' };

    return movie;
}

module.exports = {
    getMovies,
    getMovie,
    createMovie,
    updateMovie,
    deleteMovie,
};
