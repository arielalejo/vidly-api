const _ = require('lodash');
const bcrypt = require('bcrypt');
const { validateUser } = require('./requestValidation');
const { User } = require('../models/users');

async function registerUser(body) {
    const { error } = validateUser(body);
    if (error) throw { status: 400, message: error.details[0].message };

    let user = await User.findOne({ email: body.email });
    if (user) throw { status: 409, message: 'user already regitered' };

    user = new User(_.pick(body, ['name', 'password', 'email', 'isAdmin']));
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
    await user.save();

    const token = user.generateAuthenToken();

    return {
        user: _.pick(user, ['_id', 'name', 'email', 'isAdmin']),
        token,
    };
}

async function getCurrentUser(user) {
    //user._id
    const currentUser = await User.findOne({ _id: user._id }).select(
        '-password'
    );

    return currentUser;
}

module.exports = { registerUser, getCurrentUser };
