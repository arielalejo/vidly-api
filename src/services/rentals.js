const Fawn = require('fawn');
const { validateId, validateRental } = require('./requestValidation');
const { Rental } = require('../models/rentals');
const { Customer } = require('../models/customers');
const { Movie } = require('../models/movies');

async function getRentals() {
    return await Rental.find().sort({ dateOut: 1 });
}

async function getRental(id) {
    const { error } = validateId(id);
    if (error) throw { status: 400, message: 'id pattern is incorrect' };

    const rental = await Rental.findById(id);
    if (!rental)
        throw { status: 404, message: 'rental with given id not found' };
    return rental;
}

async function createRental(body) {
    const { error } = validateRental(body);
    if (error) throw { status: 400, message: error.details[0].message };

    const customer = await Customer.findOne({ name: body.customer });
    if (!customer)
        throw { status: 404, message: 'customer does not exist, create first' };

    const movie = await Movie.findOne({ title: body.movie });
    if (!movie)
        throw { status: 404, message: 'movie does not exist in our catalog' };

    const rental = new Rental({
        customer: {
            _id: customer._id,
            name: customer.name,
            phone: customer.phone,
            isGold: customer.isGold,
        },
        movie: {
            _id: movie._id,
            title: movie.title,
            dailyRentalRate: movie.dailyRentalRate,
        },
        dateOut: body.dateOut,
        dateReturned: body.dateReturned,
        rentalPay: body.rentalPay,
    });

    /* transaction */
    new Fawn.Task()
        .update(
            'movies',
            { title: movie.title },
            {
                $inc: { numberInStock: -1 },
            }
        )
        .save('rentals', rental)
        .run();
    return rental;
}

async function updateRental(id, body) {
    const { iderror } = validateId(id);
    if (iderror) throw { status: 400, message: 'id pattern is incorrect' };

    const { error } = validateRental(body);
    if (error) throw { status: 400, message: error.details[0].message };

    const rental = await Rental.findById(id);
    if (!rental)
        throw { status: 404, message: 'rental with the given id not found' };

    const movie = await Movie.findOne({ title: body.movie });
    if (!movie)
        throw { status: 404, message: 'movie does not exist in our catalog' };

    rental.set({
        customer: {
            _id: rental.customer._id,
            name: rental.customer.name,
            phone: rental.customer.phone,
            isGold: rental.customer.isGold,
        },
        movie: {
            _id: movie._id,
            title: body.movie /* do we want to update the movie ??? */,
            dailyRentalRate: movie.dailyRentalRate,
        },
        dateOut: body.dateOut,
        dateReturned: body.dateReturned,
        rentalPay: body.rentalPay ? body.rentalPay : rental.rentalPay,
    });

    return await rental.save();
}

async function deleteRental(id) {
    const { error } = validateId(id);
    if (error) throw { status: 400, message: 'id pattern is incorrect' };

    const rental = await Rental.findByIdAndDelete(id);
    if (!rental)
        throw { status: 404, message: 'rental with the given id not found' };
    return rental;
}

module.exports = {
    getRentals,
    getRental,
    createRental,
    updateRental,
    deleteRental,
};
