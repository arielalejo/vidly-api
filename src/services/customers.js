const { Customer } = require('../models/customers');
const { validateId, validateCustomer } = require('./requestValidation');

async function getCustomers() {
    return await Customer.find().sort({ name: 1 });
}

async function getCustomer(id) {
    const { error } = validateId(id);
    if (error) throw { status: '400', message: 'id pattern incorrect' };

    const customer = await Customer.findById(id);
    if (!customer)
        throw {
            status: '404',
            message: 'customer with the given id was not found',
        };

    return customer;
}

async function createCustomer(body) {
    const { error } = validateCustomer(body);
    if (error) throw { status: '400', message: error.details[0].message };

    const newCustomer = new Customer({
        name: body.name,
        phone: body.phone,
        isGold: body.isGold,
    });
    return await newCustomer.save();
}

async function updateCustomer(id, body) {
    const { iderror } = validateId(id);
    if (iderror) throw { status: '400', message: 'id pattern incorrect' };

    const { error } = validateCustomer(body);
    if (error) throw { status: '400', message: error.details[0].message };

    const customer = await Customer.findById(id);
    if (!customer)
        throw {
            status: '404',
            message: 'customer with the given id was not found',
        };

    customer.set({
        name: body.name ? body.name : customer.name,
        //phone: body.phone ? body.phone : customer.phone,
        phone: (() => {
            if (body.phone) return body.phone;
            else return customer.phone;
        })(),
        isGold: body.isGold != null ? body.isGold : customer.isGold,
    });
    return await customer.save();
}

async function deleteCustomer(id) {
    const { error } = validateId(id);
    if (error) throw { status: '400', message: 'id pattern incorrect' };

    const deletedCustomer = await Customer.findByIdAndDelete(id);
    if (!deletedCustomer)
        throw {
            status: '404',
            message: 'customer with the given id was not found',
        };

    return deletedCustomer;
}

module.exports = {
    getCustomers,
    getCustomer,
    createCustomer,
    updateCustomer,
    deleteCustomer,
};
