const Joi = require('joi');
const passwordComplexity = require('joi-password-complexity');

function validateId(id) {
    const schema = Joi.objectId().required();

    return schema.validate(id);
}

/* Genres */

function validateGenre(genre) {
    const schema = Joi.object({
        name: Joi.string().min(3).max(20).required(),
    });
    return schema.validate(genre);
}

/* Customers */

function validateCustomer(customer) {
    const schema = Joi.object({
        name: Joi.string().min(3).max(20).required(),
        phone: Joi.string().min(6).max(6),
        isGold: Joi.boolean(),
    });
    return schema.validate(customer);
}

/* Movies */
function validateMovie(movie) {
    const schema = Joi.object({
        title: Joi.string().min(3).max(50).required(),
        numberInStock: Joi.number().min(0),
        dailyRentalRate: Joi.number(),
        genreId: Joi.objectId().required(),
    });
    return schema.validate(movie);
}

/* Rentals */
function validateRental(rental) {
    const schema = Joi.object({
        customer: Joi.string().min(3).max(20).required(),
        movie: Joi.string().min(0).max(50).required(),
        rentalPay: Joi.number().min(0),
        dateOut: Joi.date(),
        dateReturned: Joi.date(),
    });
    return schema.validate(rental);
}

/* Users */
function validateUser(user) {
    let schema = Joi.object({
        name: Joi.string().min(3).max(50).required(),
        email: Joi.string().required().email(),
        password: passwordComplexity({
            min: 6,
            max: 25,
            lowerCase: 1,
            upperCase: 0,
            numeric: 0,
            symbol: 0,
            requirementCount: 4,
        }).required(),
        isAdmin: Joi.boolean().optional(),
    });

    return schema.validate(user);
}

module.exports = {
    validateId,
    validateGenre,
    validateCustomer,
    validateMovie,
    validateRental,
    validateUser,
};
