const { Genre } = require('../models/genres');
const { validateId, validateGenre } = require('./requestValidation');

async function getGenres() {
    const genres = await Genre.find().select({ __v: 0 });
    return genres;
}

async function getGenre(id) {
    const { error } = validateId(id);
    if (error) throw { status: '400', message: 'id pattern is incorrect' };

    const genre = await Genre.findById(id);
    if (!genre)
        throw { status: '404', message: 'genre with the given ID  not found' }; //throw new Error('genre with the given ID  not found');
    return genre;
}

async function createGenre(body) {
    const { error } = validateGenre(body);
    if (error) {
        throw { status: '400', message: error.details[0].message };
    }
    const newGenre = new Genre({
        name: body.name,
    });
    const result = await newGenre.save();
    return result;
}

async function updateGenre(id, body) {
    const { iderror } = validateId(id);
    if (iderror) throw { status: 400, message: 'id pattern is incorrect' };

    const { error } = validateGenre(body);
    if (error) throw { status: '400', message: error.details[0].message };

    const updatedGenre = await Genre.findByIdAndUpdate(
        id,
        {
            $set: {
                name: body.name,
            },
        },
        {
            new: true,
        }
    );
    if (!updatedGenre)
        throw { status: '404', message: 'genre with the given ID  not found' };
    return updatedGenre;
}

async function deleteGenre(id) {
    const { error } = validateId(id);
    if (error) throw { status: 400, message: 'id pattern is incorrect' };

    const deletedGenre = await Genre.findByIdAndDelete(id);
    if (!deletedGenre) throw new Error('genre with the given ID  not found');
    return deletedGenre;
}

module.exports = {
    getGenres,
    getGenre,
    createGenre,
    updateGenre,
    deleteGenre,
};
