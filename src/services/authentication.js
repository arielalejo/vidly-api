const debug = require('debug')('app:services');
const Joi = require('joi');
const passwordComplexity = require('joi-password-complexity');
const bcrypt = require('bcrypt');
const { User } = require('../models/users');

/* post a user login */
async function loginUser(body) {
    const { error } = validateUser(body);
    if (error) throw {status: 400, message:"Invalid email or password"}

    const user = await User.findOne({ email: body.email });
    if (!user) throw {status: 400, message:"Invalid email or password"}

    const validPassword = await bcrypt.compare(
        body.password,
        user.password
    );
    if (!validPassword) throw {status: 400, message:"Invalid email or password"}

    const token = user.generateAuthenToken();

    return token;
}

/* function for validating request */
function validateUser(user) {
    let schema = Joi.object({
        email: Joi.string().required().email(),
        password: passwordComplexity({
            min: 6,
            max: 25,
            lowerCase: 0,
            upperCase: 0,
            numeric: 0,
            symbol: 0,
            requirementCount: 4,
        }).required(),
    });

    return schema.validate(user);
}

// exports
module.exports = { loginUser };
