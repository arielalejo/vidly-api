const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = function (req, res, next) {
    if (!config.get('requiresAuth')) return next();

    const token = req.header('x-auth-token');
    
    if (!token) {
        res.status(401).send('Access denied. No token provided.');
        return;
    }

    /* if the client provides a valid token, it returns a json decoded, if not it will throw an exception */
    try {
        const decoded = jwt.verify(token, config.get('jwtPrivateKey'));

        /* we add a property to the 'req' object called 'user' with the decoded payload */
        req.user = decoded; /* e.g. req.user = { _id:  "ab4220f1231", ... } */
        next();
    } catch (ex) {
        res.status(400).send('Invalid Token');
    }
};
