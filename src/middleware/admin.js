const config = require("config");

function admin(req, res, next) {
	if (!config.get("requiresAuth")) return next();
	
    if (!req.user.isAdmin) {
        res.status(403).send('Acces Denied');
        return;
    }
    next();
}

module.exports = admin;
