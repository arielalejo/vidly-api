const winston = require('winston');

module.exports = function (error, req, res, next) {
    winston.error(error.message, error);

    if (error.status) {
        res.status(error.status).json(error.message);
        return;
    }
    res.status(500).send('Something went wrong');
};
 
