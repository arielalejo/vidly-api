require('express-async-errors');
const winston = require('winston');
//require('winston-mongodb');

module.exports = function () {
    /* ------------------------------------ */
    /* global exceptions/rejections handler */
    /* ------------------------------------ */

    winston.exceptions.handle(
        new winston.transports.File({
            filename: 'uncaughtExceptions.log',
            format: winston.format.combine(
                winston.format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss',
                }),
                winston.format.json()
            ),
        })
    );

    process.on('unhandledRejection', (ex) => {
        throw ex;
    });

    /* ----------------- */
    /* creating winston transports */
    /* ----------------- */
    const myFormat = winston.format.printf(
        ({ level, message, label, timestamp }) => {
            // return `${timestamp} [${label}] ${level}: ${message}`;
            return `[ ${label}] ${level}: ${message} `;
        }
    );

    winston.add(
        new winston.transports.Console({
            format: winston.format.combine(
                winston.format.label({ label: 'vidly' }),
                winston.format.colorize(),
                winston.format.prettyPrint(),
                myFormat
            ),
            handleExceptions: true,
        })
    );
    winston.add(
        new winston.transports.File({
            filename: 'logFile.log',
            format: winston.format.combine(
                winston.format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss',
                }),
                winston.format.json()
            ),
        })
    );

    // winston.add(
    //     new winston.transports.MongoDB({
    //         db: 'mongodb://localhost/vidly',
    //         options: { useUnifiedTopology: true },
    //         metaKey: 'stack',
    //     })
    // );
};
