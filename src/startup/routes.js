const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const genresRouter = require('../routes/genres');
const customerRouter = require('../routes/customers');
const moviesRouter = require('../routes/movies');
const rentalsRouter = require('../routes/rentals');
const usersRouter = require('../routes/users');
const authRouter = require('../routes/authentication');
const error = require('../middleware/error');

module.exports = function (app) {
    app.use(cors());
    app.use(express.json());
    app.use(morgan('dev'));
    app.use('/api/genres', genresRouter);
    app.use('/api/movies/', moviesRouter);
    app.use('/api/customers', customerRouter);
    app.use('/api/rentals', rentalsRouter);
    app.use('/api/users', usersRouter);
    app.use('/api/auth', authRouter);
    app.use(error);
};
