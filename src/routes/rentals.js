const { Router } = require('express');
const controller = require('../controllers/rentals');
const authoriz = require('../middleware/authorization');

const router = Router();

router.get('/', controller.getRentals);
router.get('/:id', controller.getRental);
router.post('/', authoriz, controller.createRental);
router.put('/:id', authoriz, controller.updateRental);
router.delete('/:id', authoriz, controller.deleteRental);

module.exports = router;
