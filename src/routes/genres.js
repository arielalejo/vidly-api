const { Router } = require('express');
const controller = require('../controllers/genres');
const authoriz = require('../middleware/authorization');
const admin = require('../middleware/admin');

const router = Router();

router.get('/', controller.getGenres);
router.get('/:id', controller.getGenre);
router.post('/', authoriz, controller.createGenre);
router.put('/:id', authoriz, controller.updateGenre);
router.delete('/:id', authoriz, admin, controller.deleteGenre);

module.exports = router;
