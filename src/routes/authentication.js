const { Router } = require('express');
const controller = require('../controllers/authentication');

const router = Router();

router.post('/', controller.loginUser);

module.exports = router;
