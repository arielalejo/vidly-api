const { Router } = require('express');
const controller = require('../controllers/users');
const authoriz = require('../middleware/authorization');
const router = Router();

router.post('/', controller.registerUser);
router.get('/me', authoriz, controller.getCurrentUser);
module.exports = router;
