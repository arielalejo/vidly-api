const { Router } = require('express');
const controller = require('../controllers/customers');

const router = Router();

// router.delete('/:id', async (req, res) => {
//     res.send(await deleteCustomer(req.params.id));
// });
router.get('/', controller.getCustomers);
router.get('/:id', controller.getCustomer);
router.post('/', controller.createCustomer);
router.put('/:id', controller.updateCustomer);
router.delete('/:id', controller.deleteCustomer);

module.exports = router;
