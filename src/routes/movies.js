const { Router } = require('express');
const controller = require('../controllers/movies');
const authoriz = require('../middleware/authorization');

const router = Router();

router.get('/', controller.getMovies);
router.get('/:id', controller.getMovie);
router.post('/', authoriz, controller.createMovie);
router.put('/:id', authoriz, controller.updateMovie);
router.delete('/:id', authoriz, controller.deleteMovie);

module.exports = router;
