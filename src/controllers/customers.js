const services = require('../services/customers');

async function getCustomers(req, res) {
    const customers = await services.getCustomers();
    res.send(customers);
}

async function getCustomer(req, res) {
    const id = req.params.id;
    const customer = await services.getCustomer(id);
    res.send(customer);
}

async function createCustomer(req, res) {
    const body = req.body;
    const createdCustomer = await services.createCustomer(body);
    res.send(createdCustomer);
}

async function updateCustomer(req, res) {
    const id = req.params.id;
    const body = req.body;
    const updatedCustomer = await services.updateCustomer(id, body);
    res.send(updatedCustomer);
}

async function deleteCustomer(req, res) {
    const id = req.params.id;
    const deletedCustomer = await services.deleteCustomer(id);
    res.send(deletedCustomer);
}

module.exports = {
    getCustomers,
    getCustomer,
    createCustomer,
    updateCustomer,
    deleteCustomer,
};
