// const asyncMiddleware = require('../middleware/async):
const services = require('../services/genres');

const getGenres = async function (req, res) {
    res.send(await services.getGenres());
};

const getGenre = async function (req, res) {
    res.send(await services.getGenre(req.params.id));
};

const createGenre = async function (req, res) {
    res.send(await services.createGenre(req.body));
};

const updateGenre = async function (req, res) {
    res.send(await services.updateGenre(req.params.id, req.body));
};

const deleteGenre = async function (req, res) {
    res.send(await services.deleteGenre(req.params.id));
};

module.exports = {
    getGenres,
    getGenre,
    createGenre,
    updateGenre,
    deleteGenre,
};
