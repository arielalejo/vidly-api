const services = require('../services/movies');

async function getMovies(req, res) {
    res.send(await services.getMovies());
}

async function getMovie(req, res) {
    res.send(await services.getMovie(req.params.id));
}

async function createMovie(req, res) {
    res.send(await services.createMovie(req.body));
}

async function updateMovie(req, res) {
    res.send(await services.updateMovie(req.params.id, req.body));
}

async function deleteMovie(req, res) {
    res.send(await services.deleteMovie(req.params.id));
}

module.exports = {
    getMovies,
    getMovie,
    createMovie,
    updateMovie,
    deleteMovie,
};
