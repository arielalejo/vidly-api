const services = require('../services/users');

async function registerUser(req, res) {
    const { user, token } = await services.registerUser(req.body);
    res.status(201).header('x-auth-token', token).header("access-control-expose-headers", "x-auth-token").send(user);
}

async function getCurrentUser(req, res) {
    res.send(await services.getCurrentUser(req.user));
}

module.exports = { registerUser, getCurrentUser };
