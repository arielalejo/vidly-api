const services = require('../services/authentication');
const debug = require('debug')('app:controller');

/* controllers */
async function loginUser(req, res) {
    const token = await services.loginUser(req.body);
    res.header('x-auth-token', token).header("access-control-expose-headers", "x-auth-token").send(token);
    
}

/* export controllers */
module.exports = { loginUser };
