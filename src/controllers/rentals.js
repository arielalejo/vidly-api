const debug = require('debug')('app:controller');
const services = require('../services/rentals');

async function getRentals(req, res) {
    res.send(await services.getRentals());
}

async function getRental(req, res) {
    res.send(await services.getRental(req.params.id));
}

async function createRental(req, res) {
    res.send(await services.createRental(req.body));
}

async function updateRental(req, res) {
    res.send(await services.updateRental(req.params.id, req.body));
}

async function deleteRental(req, res) {
    res.send(await services.deleteRental(req.params.id));
}

module.exports = {
    getRentals,
    getRental,
    createRental,
    updateRental,
    deleteRental,
};
