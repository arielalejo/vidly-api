const mongoose = require('mongoose');

// -------------------------------------------------------
// Schema and Model
// -------------------------------------------------------
const genreSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 20,
    },
});
const Genre = mongoose.model('Genre', genreSchema);

module.exports = {
    genreSchema,
    Genre,
};
