const { Schema, model } = require('mongoose');
const jwt = require('jsonwebtoken');
const config = require('config');

const userSchema = new Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 50,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
        minlength: 6,
        maxlength: 1024,
    },
    isAdmin: Boolean,
});

/* For adding methods to the userSchema (e.g. generateAuthenToken) */
userSchema.methods.generateAuthenToken = function () {
    return jwt.sign(
        /* in order to generate a JWT we need a PAYLOAD, and a PRIVATE KEY */
        {
            _id: this._id,
            name: this.name,
            email: this.email,
            isAdmin: this.isAdmin,
        },
        config.get('jwtPrivateKey')
    );
};

const User = model('User', userSchema);

module.exports.User = User;
