const mongoose = require('mongoose');
const { genreSchema } = require('./genres');

// schema and model
const moviesSchema = new mongoose.Schema({
    title: {
        type: String,
        minlength: 3,
        maxlength: 50,
        required: true,
    },
    numberInStock: {
        type: Number,
        min: 0,
        max: 200,
        default: 0,
    },
    dailyRentalRate: {
        type: Number,
        min: 0,
        max: (() => {
            return this.numberInStock;
        })(),
        default: 0,
    },
    genre: {
        type: genreSchema,
        required: true,
    },
});

const Movie = mongoose.model('Movie', moviesSchema);

module.exports.Movie = Movie;
