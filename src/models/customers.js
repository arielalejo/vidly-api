const mongoose = require('mongoose');

// -------------------------------------------------------
// Schema and Model
// -------------------------------------------------------
const Customer = mongoose.model(
    'Customer',
    new mongoose.Schema({
        name: {
            type: String,
            required: true,
            minlength: 3,
            maxlength: 20,
        },
        phone: {
            type: String,
            minlength: 6,
            maxlength: 6,
        },
        isGold: {
            type: Boolean,
            default: false,
        },
    })
);

module.exports.Customer = Customer;
