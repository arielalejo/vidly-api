const { Schema, model } = require('mongoose');

const rentalsSchema = new Schema({
    customer: {
        type: new Schema({
            name: {
                type: String,
                required: true,
                minlength: 3,
                maxlength: 20,
            },
            phone: {
                type: String,
                minlength: 6,
                maxlength: 6,
            },
            isGold: {
                type: Boolean,
                default: false,
            },
        }),
        required: true,
    },
    movie: {
        type: new Schema({
            title: {
                type: String,
                minlength: 3,
                maxlength: 50,
                required: true,
            },
            dailyRentalRate: {
                type: Number,
                min: 0,
                max: 200,
                default: 0,
            },
        }),
        required: true,
    },
    dateOut: {
        type: Date,
        default: new Date(),
    },
    dateReturned: Date,
    rentalPay: {
        type: Number,
        min: 0,
    },
});

const Rental = model('Rental', rentalsSchema);

module.exports.Rental = Rental;
