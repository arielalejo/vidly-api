FROM node:12.7-alpine   
WORKDIR /usr/app/vidly
COPY ./ ./
RUN npm install -S
CMD ["node", "src/index.js"]