# vidly-api

## Rest API for a movies rental application

For starting the server run:

`yarn start`

**note**: you should have a mongodb service running in your machine

The server runs in the port 3000.

**Run the services in a container**

You should have docker-compose in your machine and then run:

`docker-compose up --build`

The server will run in the port 3000.

